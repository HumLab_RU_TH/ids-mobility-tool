# Step 3 A. Install dplyr, tidyr and data.table packages

# Because the MET uses the dplyr, tidyr and data.table R-packages, it is necessary to install 
# and load these packages. The packages have to be loaded using "library" each time 
# you open the project. This can be done with each of the scripts that are part of the MET.

install.packages("dplyr")
install.packages("tidyr")
install.packages("data.table")