# Step 26. Export MEF data frame

# If necessary, the MEF can be exported for use in a different statistical package: TH testen!!

# Load packages

library(dplyr)
library(tidyr)
library(data.table)

# 26.1 Exporting data frame to text file (tab delimited)

write.table(MEF, "D:/Projects/Migration/IDS_Migration_Project_1/MEF.txt", sep="\t")


# 26.2 Exporting data frame to csv

write.table(MEF, file="MEF.csv", sep=",",row.names=F)


# 26.3 Exporting data frame to SPSS (requires foreign-package)

# 26.3.1 Install foreign package

install.packages("foreign")


# 26.3.2 Load foreign package

library(foreign)


# 26.3.3 Export to SPSS

write.foreign(MEF, "MEF.txt", "mydata.sps", package="SPSS")


# 26.4 Exporting data frame to STATA (requires foreign-package)

# 26.4.1 Install foreign package

install.packages("foreign")


# 26.4.2 Load foreign package

library(foreign)  


# 26.4.3 Export to STATA:  

write.dta(dt, "MEF.dta")


# 26.5 Exporting data frame to SAS (requires foreign-package):

# 26.5.1 Install foreign package

install.packages("foreign")


# 26.5.2 Load foreign package

library(foreign)


# 26.5.3 Export to SAS:  

write.foreign(MEF, "MEF.txt", "MEF.sas", package="SAS")