# HSN observations, issue 1: Double contexts

# In the INDIV_CONTEXT file in the HSN for a number of Id_I's there are double or more contexts with the same 
# timestamps. This means that we have to filter those records out. We do this by ordering the CONTEXT_RP data frame using 
# the date and the Id_I and then counting the number of each start date and then filtering out those records. 

# Step 1.1 

# Add Start_date columns by uniting the three date columns.
# The new columns is placed at the end of the data frame
# The new data frame is reordered

CONTEXT_RP_CHECK <- CONTEXT_RP %>% 
  unite (Start_date, Start_year, Start_month, Start_day, sep = "-", remove = FALSE) %>%
  select(-Start_date, everything()) %>% 
  arrange(Id_I, Start_date)


# Step 1.2 Count the double records 

# This for loop counts the occurrence per Id_I of each new sequence of Start_dates.

CONTEXT_RP_CHECK$Count_1 <- NA

for(i in seq_len(nrow(CONTEXT_RP_CHECK))) {
  if (i == 1 || (CONTEXT_RP_CHECK$Start_date[i] != CONTEXT_RP_CHECK$Start_date[i-1] || (CONTEXT_RP_CHECK$Id_I[i] != CONTEXT_RP_CHECK$Id_I[i-1])))
    CONTEXT_RP_CHECK$Count_1[i] <- 1
  else
    CONTEXT_RP_CHECK$Count_1[i] <- CONTEXT_RP_CHECK$Count_1[i-1] + 1
}

# Step 1.3 Filter out the double contexts

# In the last step, the double records are filtered out of the data frame
# Only the necessary records are saved
# The new data frame can be used in the Extract_migrations.R script


CONTEXT_RP_FILTERED <- CONTEXT_RP_CHECK %>% 
  filter(Count_1 == 1) %>% 
  select (-c(Start_date, Count_1))


# Step 1.4. Flag RP's for this issue

# In this step the Id_I codes of the RP's are stored to be able 
# to trace which HSN issues have the data for which RP's modified.

RP_ISSUE_1 <- CONTEXT_RP_CHECK %>% 
  filter(Count_1 > 1) %>% 
  distinct(Id_I) %>% 
  mutate(Issue_1 = "Yes")