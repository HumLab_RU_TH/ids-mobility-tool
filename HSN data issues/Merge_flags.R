# HSN 7: Combine all flags from the 6 issues 

# Step 7.1 Join the separate flags

# In this script all flags from the six scripts that handle the HSN data issues are joined into one data frame. 

# TH add: full_join(RP_ISSUE_4, by=c("Id_I")) %>%

FLAGS_RP_ISSUES_HSN <- RP_ISSUE_1 %>% 
  full_join(RP_ISSUE_2, by=c("Id_I")) %>%
  full_join(RP_ISSUE_3, by=c("Id_I")) %>%
  full_join(RP_ISSUE_4, by=c("Id_I")) %>%
  full_join(RP_ISSUE_5_A, by=c("Id_I")) %>%
  full_join(RP_ISSUE_5_B, by=c("Id_I")) %>%
  full_join(RP_ISSUE_6_A, by=c("Id_I")) %>%
  full_join(RP_ISSUE_6_B, by=c("Id_I")) 

# Step 7.2 Join the FLAGS_RP_ISSUES_HSN with the MEF

# The most logical moment to join the FLAGS_RP_ISSUES_HSN to the MEF is after the Split_Enter_Exit.R script has been executed.

MEF <- MEF %>% left_join(FLAGS_RP_ISSUES_HSN, by=c("Id_I"))