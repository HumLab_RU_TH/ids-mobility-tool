![LONGPOP logo](Image/LONGPOP_logo.png "LONGPOP logo")

Please note:

- *The [LONGPOP project](http://longpop-itn.eu/) has received funding from the European Union’s Horizon 2020 research and innovation programme under the Marie Skłodowska-Curie grant agreement No 676060.*
- *This deliverable reflects only the author's view and the Research Executive Agency is not responsible for any use that may be made of the information it contains.*

# IDS Mobility Tool (beta)

The IDS Mobility Tool was developed in the context of the LONGPOP project by the Radboud Group for Historical Demography and Family History and 
the Humanities Lab of the Radboud University. 

The IDS Mobility Tool aims to facilitate the research of historical migration with longitudinal datasets stored in the Intermediate Data Structure (IDS). 

The tool, developed for the R programming language, is split into three separate tools: 

- Migration Extraction Tool (MET): extracts the data on migration out of IDS datasets

- Migration Statistical Tool (MST): calculates migration rates

- Migration Visualisation Tool (MVT): visualises migration rates on a map


Before the extraction of the migration data can start, the user has to follow a few steps:

1.	In RStudio, a new R Studio Project needs to be started in a new folder. 

2.	Then the R scripts of the MET have to be downloaded and saved in that project folder. 

3.	The dplyr- and tidyr-packages have to be installed and loaded. This can be done manually, or by opening the **1_Install_packages.R** script and then running it. 

4.	The INDIVIDUAL, INDIV_CONTEXT, CONTEXT and CONTEXT_CONTEXT should be exported as text files into the new project folder. 

5.	Importing the tables as data frames can be done automatically by executing the **2_Import_tables_as_dataframes.R**script. 

6.	By executing the **3_User_input_type_value_rp.R** script, the user is asked to specify how the RP’s can be found in the IDS set in use. 

After these steps the process of extracting the migration data can start. It is advisable to go through the longer scripts step by step.

In the temporary IDS version of the HSN there are some data issues that interfere with the extraction process. Please view the **HSN data issues** directory. 

You can use the [flow chart](https://gitlab.com/HumLab_RU_TH/ids-mobility-tool/-/blob/master/Flowchart_IDS_Mobility_Tool_2023.jpg) to see how the sets of scripts are linked to each other, this is especially important when handling the data issues in the temporary IDS version of the HSN.


This is the sequence for the MET:

- 4_Extract_migrations.R 

- 5_Extract_individual_data.R

- 6_Modify_start_observations.R

- 7_Build_personal.R

- 8_Build_chronicle.R

- 9_Build_episode_file.R

- 10_Chronicle_migration_data.R

- 11_Create_MEF.R

- 12_Export_MEF.R


This is the sequence for the MST and MVT:

- 13_Split_enter_exit.R

- 14_Migrations.R

- 15_Obs_person_years.R

- 16_Mapping_rates.R 




---Please notice that this is a beta version--


